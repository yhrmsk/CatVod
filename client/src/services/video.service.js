import Api from './api';

export default {
    classes() {
        return Api.post('classes');
    },

    videos(cid, pg, pgSize) {
        return Api.post('videos', {cid: cid, page: pg, pageSize: pgSize, picFix: true});
    },

    video(id) {
        return Api.post('video/detail', {id: id, picFix: true});
    },

    play(id, src, index) {
        return Api.post('video/play', {id: id, src: src, index: index});
    },

    search(key, pg, pgSize) {
        return Api.post('video/search', {key: key, page: pg, pageSize: pgSize, picFix: true});
    },
};
