let express = require('express');
let path = require('path');

let app = express();

// app模板引擎
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(express.static(path.join(__dirname, 'public')));

// 初始化app
require('./libs/app.lib')(app);

module.exports = app;
