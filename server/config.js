let envType = process.env.NODE_ENV || 'production';
if (envType === 'development') {
    module.exports = require('./config/dev.config');
} else {
    module.exports = require('./config/prod.config');
}
