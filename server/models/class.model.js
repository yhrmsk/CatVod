let Sequelize = require('sequelize');
let db = require('../libs/db.lib');

let Class = db.define('class', {
    // 自增ID
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        unique: true,
        allowNull: false,
    },
    // 名称
    name: {
        type: Sequelize.STRING(50),
        allowNull: false,
    },
    // 子分类
    leaf: {
        type: Sequelize.TEXT,
        allowNull: false,
        defaultValue: '[]',
        get() {
            try {
                return JSON.parse(this.getDataValue('leaf'));
            } catch (e) {
                return [];
            }

        },
        set(val) {
            this.setDataValue('leaf', JSON.stringify(val));
        },
    },
    //显示
    visible: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true,
    },
}, {
    timestamps: false,
    // 设置索引
    indexes: [
        {
            unique: false,
            name: 'visible',
            fields: ['visible'],
        },
    ],
});

// 同步model到table 插入默认数据记录
Class.sync({alter: true}).then(async () => {
    let builtInDatas = [{
        "id": 1,
        "name": "电视剧",
        "leaf": [
            14,
            15,
            16,
            17,
            18,
        ],
    }, {
        "id": 2,
        "name": "电影",
        "leaf": [
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
        ],
    }, {
        "id": 3,
        "name": "综艺",
        "leaf": [],
    }, {
        "id": 4,
        "name": "动漫",
        "leaf": [],
    }, {
        "id": 5,
        "name": "动作片",
        "leaf": [],
    }, {
        "id": 6,
        "name": "喜剧片",
        "leaf": [],
    }, {
        "id": 7,
        "name": "爱情片",
        "leaf": [],
    }, {
        "id": 8,
        "name": "科幻片",
        "leaf": [],
    }, {
        "id": 9,
        "name": "恐怖片",
        "leaf": [],
    }, {
        "id": 10,
        "name": "剧情片",
        "leaf": [],
    }, {
        "id": 11,
        "name": "战争片",
        "leaf": [],
    }, {
        "id": 12,
        "name": "卡通片",
        "leaf": [],
    }, {
        "id": 13,
        "name": "微电影",
        "leaf": [],
    }, {
        "id": 14,
        "name": "国产剧",
        "leaf": [],
    }, {
        "id": 15,
        "name": "港台剧",
        "leaf": [],
    }, {
        "id": 16,
        "name": "日韩剧",
        "leaf": [],
    }, {
        "id": 17,
        "name": "欧美剧",
        "leaf": [],
    }, {
        "id": 18,
        "name": "泰国剧",
        "leaf": [],
    }, {
        "id": 19,
        "name": "记录片",
        "leaf": [],
    }, {
        "id": 20,
        "name": "伦理片",
        "leaf": [],
    }];
    for (let index in builtInDatas) {
        let data = builtInDatas[index];
        await Class.findOne({attributes: ['id'], where: {id: data.id}}).then((t) => {
            if (!t) {
                return Class.create(data).then(s => {
                    if (s) {
                        console.log(`分类${s.name}添加成功！`);
                    }
                }).catch(e => {
                    console.log(e);
                });
            }
        });
    }
});

module.exports = Class;
