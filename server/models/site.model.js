let Sequelize = require('sequelize');
let db = require('../libs/db.lib');

let Site = db.define('site', {
    // 站点标识 不可重复
    flag: {
        type: Sequelize.STRING(20),
        primaryKey: true,
        unique: true,
        allowNull: false,
    },
    // 名称
    name: {
        type: Sequelize.STRING(50),
        allowNull: false,
    },
    // 采集api
    api: {
        type: Sequelize.STRING(256),
        allowNull: false,
    },
    // 生效
    enable: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true,
    },
    // 分类绑定
    binds: {
        type: Sequelize.TEXT,
        get() {
            try {
                return JSON.parse(this.getDataValue('binds'));
            } catch (e) {
                return {};
            }
        },
        set(val) {
            this.setDataValue('binds', JSON.stringify(val));
        },
    },
    // 记录采集信息
    cj: {
        type: Sequelize.STRING,
        get() {
            try {
                return JSON.parse(this.getDataValue('cj'));
            } catch (e) {
                return {pg: 0};
            }
        },
        set(val) {
            this.setDataValue('cj', JSON.stringify(val));
        },
    },
}, {
    timestamps: false,
});

// 同步model到table 插入默认数据记录
Site.sync({alter: true}).then(async () => {

});

module.exports = Site;
